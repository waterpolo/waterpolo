package com.strongfellow.waterpolo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.strongfellow.waterpolo.utils.Utils;

public class TestUtils {
    public static void print(Object obj) throws JsonProcessingException {
        System.out.println(
                Utils.getObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(obj)
                );
    }
}
