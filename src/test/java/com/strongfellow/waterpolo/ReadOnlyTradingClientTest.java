package com.strongfellow.waterpolo;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.strongfellow.waterpolo.api.CompleteBalancesRequest;
import com.strongfellow.waterpolo.api.CompleteBalancesResponse;
import com.strongfellow.waterpolo.api.DepositAddressesRequest;
import com.strongfellow.waterpolo.api.DepositAddressesResponse;
import com.strongfellow.waterpolo.api.GenerateNewAddressRequest;
import com.strongfellow.waterpolo.api.GenerateNewAddressResponse;
import com.strongfellow.waterpolo.exception.InvalidNonceException;
import com.strongfellow.waterpolo.exception.WaterPoloException;

public class ReadOnlyTradingClientTest {

    private static void depositAddresses(TradingClient client, AuthCredentials credentials) throws JsonProcessingException, WaterPoloException {
        final DepositAddressesRequest request = new DepositAddressesRequest();
        final DepositAddressesResponse response = client.returnDepositAddresses(request, credentials);
        TestUtils.print(response);
    }

    private static void generateNewAddress(TradingClient client, AuthCredentials credentials) throws WaterPoloException, JsonProcessingException {
        final GenerateNewAddressRequest request = new GenerateNewAddressRequest();
        request.setCurrency("BTC");
        final GenerateNewAddressResponse response = client.generateNewAddress(request, credentials);
        TestUtils.print(response);
    }

    public static void main(String[] args) throws IOException, WaterPoloException {
        final String propertiesFilePath = args[0];
        final AuthCredentials creds = new PropertiesFileCredentialsProvider(propertiesFilePath).getCredentials();
        final ClientBuilder clientBuilder = new ClientBuilder();
        final TradingClient tradingClient = clientBuilder.getTradingClient();
        ReadOnlyTradingClientTest.depositAddresses(tradingClient, creds);

        final TradingClient badClient = new ClientBuilder().withNonceProvider(() -> System.currentTimeMillis() - 5000).getTradingClient();
        try {
            ReadOnlyTradingClientTest.returnCompleteBalances(badClient, creds);
        } catch(final InvalidNonceException t) {
            ; // expected
            System.out.println("we expected this");
        } catch(final WaterPoloException e) {
            System.out.println(e.getMessage());
        }

//        ReadOnlyTradingClientTest.returnCompleteBalances(tradingClient, creds);
//        ReadOnlyTradingClientTest.generateNewAddress(tradingClient, creds);


    }

    private static void returnCompleteBalances(TradingClient client, AuthCredentials credentials) throws WaterPoloException, JsonProcessingException {
        final CompleteBalancesRequest request = new CompleteBalancesRequest();
        request.setAccount("all");
        final CompleteBalancesResponse response = client.returnCompleteBalances(request, credentials);
        TestUtils.print(response);
    }
}
