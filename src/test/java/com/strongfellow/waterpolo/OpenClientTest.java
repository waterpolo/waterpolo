package com.strongfellow.waterpolo;

import java.io.IOException;

import org.joda.time.DateTime;
import org.joda.time.Hours;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.strongfellow.waterpolo.api.ChartDataRequest;
import com.strongfellow.waterpolo.api.ChartDataRequest.CandleStickPeriod;
import com.strongfellow.waterpolo.api.ChartDataResponse;
import com.strongfellow.waterpolo.api.CurrenciesRequest;
import com.strongfellow.waterpolo.api.CurrenciesResponse;
import com.strongfellow.waterpolo.api.OrderBookRequest;
import com.strongfellow.waterpolo.api.OrderBookResponse;
import com.strongfellow.waterpolo.api.TickerRequest;
import com.strongfellow.waterpolo.api.TickerResponse;
import com.strongfellow.waterpolo.api.TradeHistoryRequest;
import com.strongfellow.waterpolo.api.TradeHistoryResponse;
import com.strongfellow.waterpolo.api.VolumeRequest;
import com.strongfellow.waterpolo.api.VolumeResponse;
import com.strongfellow.waterpolo.exception.WaterPoloException;

public class OpenClientTest {

    private static final Logger logger = LoggerFactory.getLogger(OpenClientTest.class);
    public static void main(String[] args) throws IOException, WaterPoloException {
        final ClientBuilder clientBuilder = new ClientBuilder();
        final OpenClient openClient = clientBuilder.getOpenClient();

        OpenClientTest.testTicker(openClient);
        OpenClientTest.testVolume(openClient);
        OpenClientTest.testOrderBook(openClient);
        OpenClientTest.testTradeHistory(openClient);
        OpenClientTest.testChartData(openClient);
        OpenClientTest.testCurrencies(openClient);

        logger.info("SUCCESS");
    }

    private static void testChartData(OpenClient openClient) throws WaterPoloException, JsonProcessingException {
        final ChartDataRequest chartDataRequest = new ChartDataRequest();
        chartDataRequest.setCandlestickPeriod(CandleStickPeriod.FIVE_MINUTES);
        chartDataRequest.setCurrencyPair("BTC_ZEC");
        chartDataRequest.setStart(DateTime.now().minus(Hours.SIX));
        chartDataRequest.setEnd(DateTime.now());
        final ChartDataResponse chartDataResponse = openClient.returnChartData(chartDataRequest);
        TestUtils.print(chartDataResponse);
    }

    private static void testCurrencies(OpenClient openClient) throws JsonProcessingException, WaterPoloException {
        final CurrenciesResponse currenciesResponse = openClient.returnCurrencies(new CurrenciesRequest());
        TestUtils.print(currenciesResponse);
    }
    private static void testOrderBook(OpenClient openClient) throws WaterPoloException, JsonProcessingException {
        final OrderBookRequest orderBookRequest = new OrderBookRequest();
        orderBookRequest.setCurrencyPair("BTC_ETH");
        orderBookRequest.setDepth(100L);
        final OrderBookResponse orderBookResponse = openClient.returnOrderBook(orderBookRequest);
        TestUtils.print(orderBookResponse);
    }

    private static void testTicker(OpenClient openClient) throws JsonProcessingException, WaterPoloException {
        final TickerResponse tickerResponse = openClient.returnTicker(new TickerRequest());
        TestUtils.print(tickerResponse);
    }

    private static void testTradeHistory(OpenClient openClient) throws JsonProcessingException, WaterPoloException {
        final TradeHistoryRequest tradeHistoryRequest = new TradeHistoryRequest();
        tradeHistoryRequest.setCurrencyPair("BTC_ETH");
        tradeHistoryRequest.setStart(DateTime.now().minusMinutes(5));
        tradeHistoryRequest.setEnd(DateTime.now());
        final TradeHistoryResponse tradeHistoryResponse = openClient.returnTradeHistory(tradeHistoryRequest);
        TestUtils.print(tradeHistoryResponse);
    }

    private static void testVolume(OpenClient openClient) throws WaterPoloException, JsonProcessingException {
        final VolumeResponse volumeResponse = openClient.return24Volume(new VolumeRequest());
        TestUtils.print(volumeResponse);
    }
}
