package com.strongfellow.waterpolo;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.strongfellow.waterpolo.api.ChartDataRequest;
import com.strongfellow.waterpolo.api.ChartDataResponse;
import com.strongfellow.waterpolo.api.CurrenciesRequest;
import com.strongfellow.waterpolo.api.CurrenciesResponse;
import com.strongfellow.waterpolo.api.LoanOrdersRequest;
import com.strongfellow.waterpolo.api.LoanOrdersResponse;
import com.strongfellow.waterpolo.api.OrderBookRequest;
import com.strongfellow.waterpolo.api.OrderBookResponse;
import com.strongfellow.waterpolo.api.TickerRequest;
import com.strongfellow.waterpolo.api.TickerResponse;
import com.strongfellow.waterpolo.api.TradeHistoryRequest;
import com.strongfellow.waterpolo.api.TradeHistoryResponse;
import com.strongfellow.waterpolo.api.VolumeRequest;
import com.strongfellow.waterpolo.api.VolumeResponse;
import com.strongfellow.waterpolo.api.WaterPoloRequest;
import com.strongfellow.waterpolo.api.data.CandleStick;
import com.strongfellow.waterpolo.api.data.CurrencyDetails;
import com.strongfellow.waterpolo.api.data.CurrencyPair;
import com.strongfellow.waterpolo.api.data.Order;
import com.strongfellow.waterpolo.api.data.OrderType;
import com.strongfellow.waterpolo.api.data.TickerItem;
import com.strongfellow.waterpolo.api.data.Trade;
import com.strongfellow.waterpolo.api.data.VolumeItem;
import com.strongfellow.waterpolo.exception.NonRetryableWaterPoloException;
import com.strongfellow.waterpolo.exception.WaterPoloException;
import com.strongfellow.waterpolo.utils.ResponseParser;
import com.strongfellow.waterpolo.utils.Utils;

class DefaultOpenClient implements OpenClient {

    private static final Logger logger = LoggerFactory.getLogger(DefaultOpenClient.class);
    private static List<Order> makeOrders(OrderType orderType, JsonNode jsonNode) {
        final List<Order> result = new ArrayList<>();
        if (jsonNode != null) {
            for (final JsonNode child : jsonNode) {
                final Order order = new Order();
                order.setType(orderType);
                order.setPrice(Utils.toBigDecimal(child.get(0)));
                order.setAmount(Utils.toBigDecimal(child.get(1)));
                result.add(order);
            }
        }
        return result;
    }
    private final ObjectMapper objectMapper = Utils.getObjectMapper();

    private final HttpClient httpClient;

    private final RetryStrategy retryStrategy;

    DefaultOpenClient(HttpClient c, RetryStrategy r) {
        this.httpClient = c;
        this.retryStrategy = r;
    }

    private <T> T get(WaterPoloRequest request, ResponseParser<T> parser) throws WaterPoloException {
        final List<NameValuePair> params = new ArrayList<>(request.params());
        params.add(new BasicNameValuePair("command", request.getCommand()));
        URIBuilder builder;
        try {
            builder = new URIBuilder("https://poloniex.com/public");
        } catch (final URISyntaxException e1) {
            throw new NonRetryableWaterPoloException(e1);
        }
        builder.addParameters(params);
        URI uri;
        try {
            uri = builder.build();
        } catch (final URISyntaxException e) {
            throw new NonRetryableWaterPoloException(e);
        }
        final HttpGet get = new HttpGet(uri);
        final String uriString = uri.toString();
        return Utils.execute(this.httpClient, () -> get, this.retryStrategy, parser);
    }

    @Override
    public  VolumeResponse return24Volume(VolumeRequest request) throws WaterPoloException {
        return get(request, json -> {
            final VolumeResponse volumeResponse = new VolumeResponse();
            for (final Iterator<Entry<String, JsonNode>> iterator = json.fields(); iterator.hasNext(); ) {
                final Entry<String, JsonNode> entry = iterator.next();
                final String key = entry.getKey();
                if (key.startsWith("total")) {
                    final String currency = key.substring(5);
                    final BigDecimal total = Utils.toBigDecimal(entry.getValue());
                    volumeResponse.getTotals().put(currency, total);
                } else {
                    final VolumeItem volumeItem = new VolumeItem();
                    final CurrencyPair currencyPair = CurrencyPair.fromString(key);
                    volumeItem.setCurrencyPair(currencyPair);
                    volumeItem.setMarketVolume(Utils.toBigDecimal(entry.getValue().get(currencyPair.getMarketCurrency())));
                    volumeItem.setAssetVolume(Utils.toBigDecimal(entry.getValue().get(currencyPair.getAssetCurrency())));
                    volumeResponse.getVolumes().put(currencyPair, volumeItem);
                }
            }
            return volumeResponse;
        });
    }

    @Override
    public ChartDataResponse returnChartData(ChartDataRequest request) throws WaterPoloException {
        return get(request, json -> {
            final ChartDataResponse response = new ChartDataResponse();
            final List<CandleStick> candleSticks = new ArrayList<>();
            for (final JsonNode j : json) {
                final CandleStick candleStick = Utils.getObjectMapper().treeToValue(j, CandleStick.class);
                candleSticks.add(candleStick);
            }
            response.setCandleSticks(candleSticks);
            return response;
        });
    }

    @Override
    public CurrenciesResponse returnCurrencies(CurrenciesRequest request) throws WaterPoloException {
        return get(new CurrenciesRequest(), json -> {
            final CurrenciesResponse currenciesResponse = new CurrenciesResponse();
            final Map<String, CurrencyDetails> map = new HashMap<>();
            for (final Iterator<Entry<String, JsonNode>> iterator = json.fields(); iterator.hasNext();) {
                final Entry<String, JsonNode> entry = iterator.next();
                final String currency = entry.getKey();
                final CurrencyDetails details = Utils.getObjectMapper().treeToValue(entry.getValue(), CurrencyDetails.class);
                map.put(currency, details);
            }
            currenciesResponse.setCurencyDetails(map);
            return currenciesResponse;
        });
    }

    @Override
    public LoanOrdersResponse returnLoanOrders(LoanOrdersRequest request) throws WaterPoloException {
        final LoanOrdersResponse response = get(request, Utils.simpleResponseParser(LoanOrdersResponse.class));
        return response;
    }

    @Override
    public OrderBookResponse returnOrderBook(OrderBookRequest request) throws WaterPoloException {
        return get(request, json -> {
            final OrderBookResponse response = new OrderBookResponse();
            response.setSeq(json.get("seq").asLong());
            response.setFrozen(json.get("isFrozen").asInt() == 1);
            response.setAsks(DefaultOpenClient.makeOrders(OrderType.ask, json.get("asks")));
            response.setBids(DefaultOpenClient.makeOrders(OrderType.bid, json.get("bids")));
            return response;
        });
    }

    @Override
    public TickerResponse returnTicker(TickerRequest tickerRequest) throws WaterPoloException {
        return get(tickerRequest, json -> {
            final TickerResponse response = new TickerResponse();

            final List<TickerItem> tickers = new ArrayList<>();
            response.setTickers(tickers);
            for (final Iterator<Entry<String, JsonNode>> iterator = json.fields(); iterator.hasNext(); ) {
                final Entry<String, JsonNode> entry = iterator.next();
                final TickerItem item = DefaultOpenClient.this.objectMapper.treeToValue(entry.getValue(), TickerItem.class);
                item.setCurrencyPair(entry.getKey());
                tickers.add(item);
            }
            return response;
        });
    }

    @Override
    public TradeHistoryResponse returnTradeHistory(TradeHistoryRequest request) throws WaterPoloException {
        return get(request, json -> {
            final TradeHistoryResponse tradeHistoryResponse = new TradeHistoryResponse();
            final List<Trade> trades = new ArrayList<>();

            for (final JsonNode j : json) {
                final Trade trade = Utils.getObjectMapper().treeToValue(j, Trade.class);
                trades.add(trade);
            }
            tradeHistoryResponse.setTrades(trades);
            return tradeHistoryResponse;
        });
    }

}
