package com.strongfellow.waterpolo;

import com.strongfellow.waterpolo.api.ChartDataRequest;
import com.strongfellow.waterpolo.api.ChartDataResponse;
import com.strongfellow.waterpolo.api.CurrenciesRequest;
import com.strongfellow.waterpolo.api.CurrenciesResponse;
import com.strongfellow.waterpolo.api.LoanOrdersRequest;
import com.strongfellow.waterpolo.api.LoanOrdersResponse;
import com.strongfellow.waterpolo.api.OrderBookRequest;
import com.strongfellow.waterpolo.api.OrderBookResponse;
import com.strongfellow.waterpolo.api.TickerRequest;
import com.strongfellow.waterpolo.api.TickerResponse;
import com.strongfellow.waterpolo.api.TradeHistoryRequest;
import com.strongfellow.waterpolo.api.TradeHistoryResponse;
import com.strongfellow.waterpolo.api.VolumeRequest;
import com.strongfellow.waterpolo.api.VolumeResponse;
import com.strongfellow.waterpolo.exception.WaterPoloException;

public interface OpenClient {

    VolumeResponse return24Volume(VolumeRequest request)
        throws WaterPoloException;

    ChartDataResponse returnChartData(ChartDataRequest request)
        throws WaterPoloException;

    CurrenciesResponse returnCurrencies(CurrenciesRequest request)
        throws WaterPoloException;

    LoanOrdersResponse returnLoanOrders(LoanOrdersRequest request)
        throws WaterPoloException;

    OrderBookResponse returnOrderBook(OrderBookRequest request)
        throws WaterPoloException;

    TickerResponse returnTicker(TickerRequest tickerRequest)
        throws WaterPoloException;

    TradeHistoryResponse returnTradeHistory(TradeHistoryRequest request)
        throws WaterPoloException;

}
