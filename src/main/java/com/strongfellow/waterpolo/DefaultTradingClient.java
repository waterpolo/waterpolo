package com.strongfellow.waterpolo;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.strongfellow.waterpolo.api.ActiveLoansRequest;
import com.strongfellow.waterpolo.api.ActiveLoansResponse;
import com.strongfellow.waterpolo.api.AvailableAccountBalancesRequest;
import com.strongfellow.waterpolo.api.AvailableAccountBalancesResponse;
import com.strongfellow.waterpolo.api.BuyRequest;
import com.strongfellow.waterpolo.api.BuyResponse;
import com.strongfellow.waterpolo.api.CancelLoanOfferRequest;
import com.strongfellow.waterpolo.api.CancelLoanOfferResponse;
import com.strongfellow.waterpolo.api.CancelOrderRequest;
import com.strongfellow.waterpolo.api.CancelOrderResponse;
import com.strongfellow.waterpolo.api.CompleteBalancesRequest;
import com.strongfellow.waterpolo.api.CompleteBalancesResponse;
import com.strongfellow.waterpolo.api.CreateLoanOfferRequest;
import com.strongfellow.waterpolo.api.CreateLoanOfferResponse;
import com.strongfellow.waterpolo.api.DepositAddressesRequest;
import com.strongfellow.waterpolo.api.DepositAddressesResponse;
import com.strongfellow.waterpolo.api.DepositsWithdrawalsRequest;
import com.strongfellow.waterpolo.api.DepositsWithdrawalsResponse;
import com.strongfellow.waterpolo.api.FeeInfoRequest;
import com.strongfellow.waterpolo.api.FeeInfoResponse;
import com.strongfellow.waterpolo.api.GenerateNewAddressRequest;
import com.strongfellow.waterpolo.api.GenerateNewAddressResponse;
import com.strongfellow.waterpolo.api.LendingHistoryRequest;
import com.strongfellow.waterpolo.api.LendingHistoryResponse;
import com.strongfellow.waterpolo.api.MoveOrderRequest;
import com.strongfellow.waterpolo.api.MoveOrderResponse;
import com.strongfellow.waterpolo.api.OpenLoanOffersRequest;
import com.strongfellow.waterpolo.api.OpenLoanOffersResponse;
import com.strongfellow.waterpolo.api.OpenOrdersRequest;
import com.strongfellow.waterpolo.api.OpenOrdersResponse;
import com.strongfellow.waterpolo.api.OrderTradeRequest;
import com.strongfellow.waterpolo.api.OrderTradesResponse;
import com.strongfellow.waterpolo.api.SellRequest;
import com.strongfellow.waterpolo.api.SellResponse;
import com.strongfellow.waterpolo.api.ToggleAutoRenewRequest;
import com.strongfellow.waterpolo.api.ToggleAutoRenewResponse;
import com.strongfellow.waterpolo.api.TradeHistoryRequest;
import com.strongfellow.waterpolo.api.TradeHistoryResponse;
import com.strongfellow.waterpolo.api.WaterPoloRequest;
import com.strongfellow.waterpolo.api.WithdrawRequest;
import com.strongfellow.waterpolo.api.WithdrawResponse;
import com.strongfellow.waterpolo.api.data.ActiveLoan;
import com.strongfellow.waterpolo.api.data.CompleteBalanceSummary;
import com.strongfellow.waterpolo.api.data.LoanHistory;
import com.strongfellow.waterpolo.api.data.LoanOffer;
import com.strongfellow.waterpolo.exception.NonRetryableWaterPoloException;
import com.strongfellow.waterpolo.exception.WaterPoloException;
import com.strongfellow.waterpolo.utils.HmacSha512Signer;
import com.strongfellow.waterpolo.utils.ResponseParser;
import com.strongfellow.waterpolo.utils.Utils;

class DefaultTradingClient implements TradingClient {

    private static final Logger logger = LoggerFactory.getLogger(DefaultTradingClient.class);
    private static final Map<String, BigDecimal> EMPTY_MAP = new HashMap<>();

    private static final Map<String, BigDecimal> currencyMap(JsonNode json) {
            final Map<String, BigDecimal> map = new LinkedHashMap<>();
            for (final Iterator<Entry<String, JsonNode>> iterator = json.fields(); iterator.hasNext(); ) {
                final Entry<String, JsonNode> entry = iterator.next();
                final String currency = entry.getKey();
                final BigDecimal balance = Utils.toBigDecimal(entry.getValue());
                map.put(currency, balance);
            }
            return map;
        }

    private final HttpClient httpClient;
    private final NonceProvider nonceProvider;

    private final ObjectMapper objectMapper = Utils.getObjectMapper();
    private final RetryStrategy retryStrategy;

    DefaultTradingClient(HttpClient h,
            NonceProvider n,
            RetryStrategy r) {
        this.httpClient = h;
        this.nonceProvider = n;
        this.retryStrategy = r;
    }

    @Override
    public BuyResponse buy(BuyRequest request, AuthCredentials credentials) throws WaterPoloException {
        throw new UnsupportedOperationException();
    }

    @Override
    public CancelLoanOfferResponse cancelLoanOffer(CancelLoanOfferRequest request, AuthCredentials credentials) throws WaterPoloException {
        return post(request, credentials, Utils.simpleResponseParser(CancelLoanOfferResponse.class));
    }

    @Override
    public CancelOrderResponse cancelOrder(CancelOrderRequest request, AuthCredentials credentials) throws WaterPoloException {
        throw new UnsupportedOperationException();
    }

    @Override
    public CreateLoanOfferResponse createLoanOffer(CreateLoanOfferRequest request, AuthCredentials credentials) throws WaterPoloException {
        return post(request, credentials, Utils.simpleResponseParser(CreateLoanOfferResponse.class));
    }

    @Override
    public GenerateNewAddressResponse generateNewAddress(GenerateNewAddressRequest request, AuthCredentials credentials) throws WaterPoloException {
        return post(request, credentials, Utils.simpleResponseParser(GenerateNewAddressResponse.class));
    }

    @Override
    public MoveOrderResponse moveOrder(MoveOrderRequest request, AuthCredentials credentials) throws WaterPoloException {
        throw new UnsupportedOperationException();
    }

    private <T> T post(WaterPoloRequest request, AuthCredentials credentials, ResponseParser<T> parser) throws WaterPoloException  {
        final HttpRequestGenerator generator = () -> {
            final String url = "https://poloniex.com/tradingApi";
            logger.info("posting to {}", url);

            final String nonce = String.valueOf(DefaultTradingClient.this.nonceProvider.get());
            final List<NameValuePair> params = new ArrayList<>(request.params());
            params.add(new BasicNameValuePair("command", request.getCommand()));
            params.add(new BasicNameValuePair("nonce", nonce));
            final HttpPost post = new HttpPost(url);
            HttpEntity requestEntity;
            try {
                requestEntity = new UrlEncodedFormEntity(params);
            } catch (final UnsupportedEncodingException e11) {
                throw new NonRetryableWaterPoloException(e11);
            }
            post.setEntity(requestEntity);
            String requestContent;
            try {
                requestContent = EntityUtils.toString(requestEntity);
            } catch (ParseException | IOException e12) {
                throw new NonRetryableWaterPoloException(e12);
            }
            logger.info("post {}", requestContent);
            final String signature = HmacSha512Signer.sign(requestContent, credentials.getSecret());
            post.addHeader("Key", credentials.getKey());
            post.addHeader("Sign", signature);
            return post;
        };
        return Utils.execute(this.httpClient, generator, this.retryStrategy, parser);
    }

    @Override
    public ActiveLoansResponse returnActiveLoans(ActiveLoansRequest request, AuthCredentials credentials) throws WaterPoloException {
        return post(request, credentials, json -> {
            final JsonNode provided = json.get("provided");
            final ActiveLoansResponse response = new ActiveLoansResponse();
            final List<ActiveLoan> activeLoans = new ArrayList<>();
            response.setProvided(activeLoans);
            for (final Iterator<JsonNode> iterator = provided.iterator(); iterator.hasNext();) {
                activeLoans.add(DefaultTradingClient.this.objectMapper.treeToValue(iterator.next(), ActiveLoan.class));
            }
            return response;
        });
    }

    @Override
    public AvailableAccountBalancesResponse returnAvailableAccountBalances(AvailableAccountBalancesRequest request, AuthCredentials credentials) throws WaterPoloException {
        return post(request, credentials, json -> {
            final AvailableAccountBalancesResponse response = new AvailableAccountBalancesResponse();
            if (!json.has("lending") || json.get("lending").isArray()) {
                response.setLending(EMPTY_MAP);
            } else {
                response.setLending(DefaultTradingClient.currencyMap(json.get("lending")));
            }
            if (!json.has("exchange") || json.get("exchange").isArray()) {
                response.setExchange(EMPTY_MAP);
            } else {
                response.setExchange(DefaultTradingClient.currencyMap(json.get("exchange")));
            }
            if (!json.has("margin") || json.get("margin").isArray()) {
                response.setMargin(EMPTY_MAP);
            } else {
                response.setMargin(DefaultTradingClient.currencyMap(json.get("margin")));
            }
            return response;
        });
    }

    @Override
    public CompleteBalancesResponse returnCompleteBalances(CompleteBalancesRequest request, AuthCredentials credentials) throws WaterPoloException {
        return post(request, credentials, json -> {
            final CompleteBalancesResponse response = new CompleteBalancesResponse();
            for (final Iterator<Entry<String, JsonNode>> iterator = json.fields(); iterator.hasNext();) {
                final Entry<String, JsonNode> next = iterator.next();
                ;
                response.getBalances().put(next.getKey(),
                        this.objectMapper.treeToValue(next.getValue(), CompleteBalanceSummary.class));
            }
            return response;
        });
    }

    @Override
    public DepositAddressesResponse returnDepositAddresses(DepositAddressesRequest request, AuthCredentials credentials) throws WaterPoloException {
        return post(request, credentials, Utils.simpleResponseParser(DepositAddressesResponse.class));
    }

    @Override
    public DepositsWithdrawalsResponse returnDepositsWithdrawals(DepositsWithdrawalsRequest request, AuthCredentials credentials) throws WaterPoloException {
        throw new UnsupportedOperationException();
    }

    @Override
    public FeeInfoResponse returnFeeInfo(FeeInfoRequest request, AuthCredentials credentials) throws WaterPoloException {
        throw new UnsupportedOperationException();
    }

    @Override
    public LendingHistoryResponse returnLendingHistory(LendingHistoryRequest request, AuthCredentials credentials) throws WaterPoloException {
        return post(request, credentials, json -> {
            final List<LoanHistory> history = new ArrayList<>();
            for (final Iterator<JsonNode> iterator = json.iterator(); iterator.hasNext(); ) {
                final LoanHistory loanHistory = DefaultTradingClient.this.objectMapper.treeToValue(iterator.next(), LoanHistory.class);
                history.add(loanHistory);
            }
            final LendingHistoryResponse response = new LendingHistoryResponse();
            response.setHistory(history);
            return response;
        });
    }

    @Override
    public OpenLoanOffersResponse returnOpenLoanOffers(OpenLoanOffersRequest request, AuthCredentials credentials) throws WaterPoloException {
        return post(request, credentials, json -> {
                final OpenLoanOffersResponse response = new OpenLoanOffersResponse();
                for (final Iterator<Entry<String, JsonNode>> iterator = json.fields(); iterator.hasNext(); ) {
                    final Entry<String, JsonNode> entry = iterator.next();
                    final String currency = entry.getKey();
                    final LoanOffer[] offers = DefaultTradingClient.this.objectMapper.treeToValue(entry.getValue(), LoanOffer[].class);
                    response.getOffers().put(currency, Arrays.asList(offers));
                }
                return response;
            });
    }

    @Override
    public OpenOrdersResponse returnOpenOrders(OpenOrdersRequest request, AuthCredentials credentials) throws WaterPoloException {
        throw new UnsupportedOperationException();
    }

    @Override
    public OrderTradesResponse returnOrderTrades(OrderTradeRequest request, AuthCredentials credentials) {
        throw new UnsupportedOperationException();
    }

    @Override
    public TradeHistoryResponse returnTradeHistory(TradeHistoryRequest request, AuthCredentials credentials) throws WaterPoloException {
        throw new UnsupportedOperationException();
    }

    @Override
    public SellResponse sell(SellRequest request, AuthCredentials credentials) throws WaterPoloException {
        throw new UnsupportedOperationException();
    }

    @Override
    public ToggleAutoRenewResponse toggleAutoRenew(ToggleAutoRenewRequest request, AuthCredentials credentials) throws WaterPoloException {
        return post(request, credentials, Utils.simpleResponseParser(ToggleAutoRenewResponse.class));
    }

    @Override
    public WithdrawResponse withdraw(WithdrawRequest request, AuthCredentials credentials) throws WaterPoloException {
        throw new UnsupportedOperationException();
    }

}
