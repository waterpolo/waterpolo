package com.strongfellow.waterpolo;

public class AuthCredentials {

    public AuthCredentials(String key, String secret) {
        this.key = key;
        this.secret = secret;
    }
    
    private final String key;
    private final String secret;
    public String getKey() {
        return key;
    }
    public String getSecret() {
        return secret;
    }
}
