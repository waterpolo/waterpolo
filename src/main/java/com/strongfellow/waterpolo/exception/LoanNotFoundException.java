package com.strongfellow.waterpolo.exception;

public class LoanNotFoundException extends PoloniexErrorException {

    private static final long serialVersionUID = 1L;

    public LoanNotFoundException(PoloniexError poloniexError) {
        super(poloniexError);
    }

    @Override
    public boolean isRetryable() {
        return false;
    }

}
