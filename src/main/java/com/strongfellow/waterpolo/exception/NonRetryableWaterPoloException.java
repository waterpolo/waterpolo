package com.strongfellow.waterpolo.exception;

public class NonRetryableWaterPoloException extends WaterPoloException {

    private static final long serialVersionUID = 1L;

    public NonRetryableWaterPoloException(Throwable t) {
        super(t);
    }

    @Override
    public boolean isRetryable() {
        return false;
    }

}
