package com.strongfellow.waterpolo.exception;

public class RetryableWaterPoloException extends WaterPoloException {

    private static final long serialVersionUID = 1L;

    public RetryableWaterPoloException(Throwable t) {
        super(t);
    }

    @Override
    public boolean isRetryable() {
        return true;
    }

}
