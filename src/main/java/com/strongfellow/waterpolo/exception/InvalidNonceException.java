
package com.strongfellow.waterpolo.exception;

public class InvalidNonceException extends PoloniexErrorException {

    private static final long serialVersionUID = 1L;

    public InvalidNonceException(PoloniexError poloniexError) {
        super(poloniexError);
    }

    @Override
    public boolean isRetryable() {
        return true;
    }

}
