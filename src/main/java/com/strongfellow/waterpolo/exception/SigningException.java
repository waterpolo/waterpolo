package com.strongfellow.waterpolo.exception;

public class SigningException extends WaterPoloException {

    private static final long serialVersionUID = 1L;

    public SigningException(Throwable t) {
        super(t);
    }

    @Override
    public boolean isRetryable() {
        return false;
    }
}
