package com.strongfellow.waterpolo.exception;

public abstract class PoloniexErrorException extends WaterPoloException {

    private static final long serialVersionUID = 1L;
    private final PoloniexError error;

    public PoloniexErrorException(PoloniexError error) {
        super(error.getError());
        this.error = error;
    }

}
