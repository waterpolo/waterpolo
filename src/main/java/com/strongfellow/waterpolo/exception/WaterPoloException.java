package com.strongfellow.waterpolo.exception;

public abstract class WaterPoloException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -3613310598042200637L;

    public WaterPoloException(String message) {
        super(message);
    }

    public WaterPoloException(Throwable t) {
        super(t);
    }

    public abstract boolean isRetryable();
}
