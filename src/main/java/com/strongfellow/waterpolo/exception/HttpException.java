package com.strongfellow.waterpolo.exception;

public class HttpException extends WaterPoloException {

    private static final long serialVersionUID = 1L;
    private final int statusCode;

    public HttpException(int statusCode, String message) {
        super(String.format("HTTP %d - %s", statusCode, message));
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return this.statusCode;
    }
    @Override
    public boolean isRetryable() {
        if (this.statusCode == 422) {
            return true;
        }
        if (this.statusCode == 429) { // too many requests
            return true;
        }
        return this.statusCode >= 500 && this.statusCode < 600;
    }

}
