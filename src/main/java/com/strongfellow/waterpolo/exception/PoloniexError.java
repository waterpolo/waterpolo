package com.strongfellow.waterpolo.exception;

public class PoloniexError {

    private String error;
    private Integer success;
    private Integer errorCode;

    public String getError() {
        return this.error;
    }

    public Integer getErrorCode() {
        return this.errorCode;
    }

    public Integer getSuccess() {
        return this.success;
    }

    public void setError(String error) {
        this.error = error;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }
}
