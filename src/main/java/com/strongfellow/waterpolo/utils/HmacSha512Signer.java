package com.strongfellow.waterpolo.utils;

import java.nio.charset.StandardCharsets;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;

import com.strongfellow.waterpolo.exception.SigningException;

public class HmacSha512Signer {

    public static String sign(String message, String secret) throws SigningException {
        try {
            final Mac shaMac = Mac.getInstance("HmacSHA512");
            final SecretKeySpec keySpec = new SecretKeySpec(secret.getBytes(StandardCharsets.UTF_8), "HmacSHA512");
            shaMac.init(keySpec);
            final byte[] macData = shaMac.doFinal(message.getBytes(StandardCharsets.UTF_8));
            final String sign = Hex.encodeHexString(macData);
            return sign;
        } catch(final Throwable t) {
            throw new SigningException(t);
        }
    }

}
