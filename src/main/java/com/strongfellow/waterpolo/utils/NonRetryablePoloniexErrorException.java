package com.strongfellow.waterpolo.utils;

import com.strongfellow.waterpolo.exception.PoloniexError;
import com.strongfellow.waterpolo.exception.PoloniexErrorException;

public class NonRetryablePoloniexErrorException extends PoloniexErrorException {

    private static final long serialVersionUID = 1L;

    public NonRetryablePoloniexErrorException(PoloniexError error) {
        super(error);
    }

    @Override
    public boolean isRetryable() {
        return false;
    }

}
