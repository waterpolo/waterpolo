package com.strongfellow.waterpolo.utils;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.strongfellow.waterpolo.HttpRequestGenerator;
import com.strongfellow.waterpolo.RetryDecision;
import com.strongfellow.waterpolo.RetryStrategy;
import com.strongfellow.waterpolo.exception.HttpException;
import com.strongfellow.waterpolo.exception.InvalidNonceException;
import com.strongfellow.waterpolo.exception.LoanNotFoundException;
import com.strongfellow.waterpolo.exception.NonRetryableWaterPoloException;
import com.strongfellow.waterpolo.exception.PoloniexError;
import com.strongfellow.waterpolo.exception.RetryableWaterPoloException;
import com.strongfellow.waterpolo.exception.WaterPoloException;

public class Utils {

    private static final Logger logger = LoggerFactory.getLogger(Utils.class);
    private static final ObjectMapper mapper = Utils.objectMapper();

    private static Pattern NONCE_PATTERN = Pattern.compile(
            "^Nonce must be greater than (\\d+)\\. You provided (.*?)\\.$");

    public static <T> T execute(HttpClient httpClient, HttpRequestGenerator generator, RetryStrategy retryStrategy, ResponseParser<T> parser) throws WaterPoloException {
        int attempts = 0;
        while (true) {
            try {
                return Utils.executeOnce(httpClient, generator, parser);
            } catch(final WaterPoloException e) {
                final RetryDecision retryDecision = retryStrategy.test(e, attempts);
                if (retryDecision.isRetry()) {
                    logger.info("decision is YES retry on attempt {}; exception message was {}", attempts, e.getMessage());
                    try {
                        final long backoff = retryDecision.getBackoff();
                        logger.info("backing off for {} ms", backoff);
                        Thread.sleep(backoff);
                    } catch (final InterruptedException e1) {
                        logger.error("interrupted while backing off; aborting retry");
                        throw(e);
                    }
                } else {
                    logger.error("decision is NO retry on attempt {}", attempts);
                    throw e;
                }
            }
            attempts++;
        }
    }

    private static <T> T executeOnce(
            HttpClient httpClient,
            HttpRequestGenerator generator,
            ResponseParser<T> parser) throws WaterPoloException {
        final HttpUriRequest request = generator.generate();
        logger.info("http {} {}", request.getMethod(), request.getURI().toString());
        HttpResponse response;
        try {
            response = httpClient.execute(request);
        } catch (final ClientProtocolException e) {
            throw new RetryableWaterPoloException(e);
        } catch (final IOException e) {
            throw new RetryableWaterPoloException(e);
        }

        logger.info("status for {} is {}", request.getURI().toString(), response.getStatusLine());
        Utils.maybeThrowHttpException(response.getStatusLine());
        String content;
        try {
            content = EntityUtils.toString(response.getEntity());
        } catch (final IOException e) {
            throw new NonRetryableWaterPoloException(e);
        }

        final JsonNode json = Utils.maybeParseJson(content);
        Utils.maybeThrowException(json);
        try {
            return parser.parse(json);
        } catch (final JsonProcessingException e) {
            throw new NonRetryableWaterPoloException(e);
        }
    }

    public static ObjectMapper getObjectMapper() {
        return mapper;
    }

    private static JsonNode maybeParseJson(String content) throws RetryableWaterPoloException {
        JsonNode json;
        try {
            json = mapper.readTree(content);
        } catch (final Throwable e) {
            throw new RetryableWaterPoloException(e);
        }
        return json;
    }
    private static void maybeThrowException(JsonNode json) throws WaterPoloException {
        if (!json.has("error")) {
            return;
        }
        PoloniexError poloniexError;
        try {
            poloniexError = mapper.treeToValue(json, PoloniexError.class);
        } catch (final JsonProcessingException e) {
            throw new RetryableWaterPoloException(e);
        }
        final String errorMessage = poloniexError.getError();

        final Matcher matcher = NONCE_PATTERN.matcher(errorMessage);
        if (matcher.matches()) {
            throw new InvalidNonceException(poloniexError);
        }

        if ("Error canceling loan order, or you are not the person who placed it.".equals(errorMessage)) {
            throw new LoanNotFoundException(poloniexError);
        }

        // if we are getting exceptions that we need to retry
        // add if clauses here
        throw new NonRetryablePoloniexErrorException(poloniexError);
    }

    private static void maybeThrowHttpException(StatusLine statusLine) throws HttpException {
        if (statusLine.getStatusCode() < 200 || statusLine.getStatusCode() >= 300) {
            throw new HttpException(statusLine.getStatusCode(), statusLine.getReasonPhrase());
        }
    }

    private static ObjectMapper objectMapper() {
        final ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JodaModule());
        mapper.enable(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS);
        final SimpleModule module = new SimpleModule();
        module.addSerializer(BigDecimal.class, new ToStringSerializer());
        mapper.registerModule(module);
        return mapper;
    }

    public static <T> ResponseParser<T> simpleResponseParser(Class<T> clazz) {
        return json -> Utils.getObjectMapper().treeToValue(json, clazz);
    }

    public static BigDecimal toBigDecimal(JsonNode jsonNode) {
        return BigDecimal.valueOf(jsonNode.asDouble());
    }
}
