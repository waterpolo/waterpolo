package com.strongfellow.waterpolo.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;

public interface ResponseParser<T> {

    T parse(JsonNode json) throws JsonProcessingException;
}
