package com.strongfellow.waterpolo;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesFileCredentialsProvider {

    private AuthCredentials credentials;

    public PropertiesFileCredentialsProvider(String propertiesFilePath) throws IOException {
        Properties properties = new Properties();
        InputStream in = new FileInputStream(propertiesFilePath);
        properties.load(in);
        
        String key = properties.getProperty("key");
        String secret = properties.getProperty("secret");
        this.credentials = new AuthCredentials(key, secret);
    }
    public AuthCredentials getCredentials() {
        return this.credentials;
    }
}
