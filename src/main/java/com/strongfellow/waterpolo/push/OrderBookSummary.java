package com.strongfellow.waterpolo.push;

import com.strongfellow.waterpolo.api.data.OrderSummary;

public interface OrderBookSummary {
    Iterable<OrderSummary> getAsks();
    Iterable<OrderSummary> getBids();
}
