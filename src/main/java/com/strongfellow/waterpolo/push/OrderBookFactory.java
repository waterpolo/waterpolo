package com.strongfellow.waterpolo.push;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.strongfellow.waterpolo.OpenClient;
import com.strongfellow.waterpolo.api.OrderBookRequest;
import com.strongfellow.waterpolo.api.OrderBookResponse;
import com.strongfellow.waterpolo.api.data.Trade;
import com.strongfellow.waterpolo.exception.WaterPoloException;
import com.strongfellow.waterpolo.utils.Utils;

import rx.Subscription;
import ws.wamp.jawampa.WampClient;
import ws.wamp.jawampa.WampClientBuilder;
import ws.wamp.jawampa.transport.netty.NettyWampClientConnectorProvider;

public class OrderBookFactory {

    private static final Logger logger = LoggerFactory.getLogger(OrderBookFactory.class);

    private OpenClient openClient;
    private String currencyPair;
    private final OrderBook orderBook;

    public OrderBookFactory() {
        this.orderBook = new OrderBook();
    }

    public OrderBook build() throws WaterPoloException {
        start();
        final WampClient client;
        try {
            final WampClientBuilder builder = new WampClientBuilder();
            builder.withConnectorProvider(new NettyWampClientConnectorProvider())
            .withUri("wss://api.poloniex.com")
            .withRealm("realm1")
            .withInfiniteReconnects()
            .withReconnectInterval(5, TimeUnit.SECONDS);
            // Create a client through the builder. This will not immediatly start
            // a connection attempt
            client = builder.build();
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
        client.statusChanged().subscribe(t1 -> {
            if (t1 instanceof WampClient.ConnectedState) {
                final Subscription zcashSubscription = client.makeSubscription(this.currencyPair)
                        .subscribe((s) -> {
                            final ArrayNode updates = s.arguments();
                            final ObjectNode keywords = s.keywordArguments();
                            final Long seq = keywords.get("seq").asLong();
                            final List<AmountProvider> items = new ArrayList<>();
                            for (final Iterator<JsonNode> iterator = updates.iterator(); iterator.hasNext(); ) {
                                final JsonNode node = iterator.next();
                                final String type = node.get("type").asText();
                                final JsonNode data = node.get("data");

                                try {
                                    switch(type) {
                                    case "orderBookModify":
                                        final OrderBookModifyItem orderBookModifyItem = Utils.getObjectMapper().treeToValue(data, OrderBookModifyItem.class);
                                        items.add(orderBookModifyItem);
                                        break;
                                    case "orderBookRemove":
                                        final OrderBookRemoveItem orderBookRemoveItem = Utils.getObjectMapper().treeToValue(data, OrderBookRemoveItem.class);
                                        items.add(orderBookRemoveItem);
                                        break;
                                    case "newTrade":
                                        final Trade newTradeItem = Utils.getObjectMapper().treeToValue(data, Trade.class);
//                                            items.add(newTradeItem);
                                        break;
                                    default:
                                       throw new RuntimeException("bad type " + type);
                                    }
                                } catch(final JsonProcessingException j) {
                                    OrderBookFactory.logger.error("json error " + node.toString(), j);
                                    throw new RuntimeException(j);
                                }
                            }
                            OrderBookFactory.this.orderBook.processItems(seq, items);
                        }, (e) -> {
                            OrderBookFactory.logger.error("exception", e);
                            System.exit(1);
                        });
            }
        });
        client.open();
        return this.orderBook;
    }

    private void start() throws WaterPoloException {
        final OrderBookRequest orderBookRequest = new OrderBookRequest();
        orderBookRequest.setCurrencyPair(this.currencyPair);
        final OrderBookResponse response = this.openClient.returnOrderBook(orderBookRequest);
        this.orderBook.initialize(response.getSeq(), response.getAsks(), response.getBids());
    }
    public OrderBookFactory withClient(OpenClient openClient) {
        this.openClient = openClient;
        return this;
    }


    public OrderBookFactory withCurrencyPair(String currencyPair) {
        this.currencyPair = currencyPair;
        return this;
    }


}
