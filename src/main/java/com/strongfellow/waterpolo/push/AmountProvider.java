package com.strongfellow.waterpolo.push;

import java.math.BigDecimal;

import com.strongfellow.waterpolo.api.data.OrderType;

public interface AmountProvider {
    BigDecimal getAmount();
    BigDecimal getRate();
    OrderType getType();
}
