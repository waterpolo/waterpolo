package com.strongfellow.waterpolo.push;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.strongfellow.waterpolo.api.data.Order;
import com.strongfellow.waterpolo.api.data.OrderSummary;
import com.strongfellow.waterpolo.api.data.OrderType;

public class OrderBook {

    public static final class BigDecimalWithSeq {
        public BigDecimal amount;
        public Long seq;
    }

    private static final Logger logger = LoggerFactory.getLogger(OrderBook.class);
    private static void put(Map<BigDecimal, BigDecimalWithSeq> map, BigDecimal price, BigDecimal amount, Long seq) {
        if (!map.containsKey(price)) {
            final BigDecimalWithSeq dws = new BigDecimalWithSeq();
            dws.amount = amount;
            dws.seq = seq;
            map.put(price, dws);
        } else {
            final BigDecimalWithSeq dws = map.get(price);
            if (dws.seq <= seq) {
                dws.amount = amount;
            }
        }
    }
    private final ConcurrentSkipListMap<BigDecimal, BigDecimalWithSeq> asks = new ConcurrentSkipListMap<>();

    private final ConcurrentSkipListMap<BigDecimal, BigDecimalWithSeq> bids = new ConcurrentSkipListMap<>(Collections.reverseOrder());

    public OrderBookSummary getSummary(BigDecimal precision, BigDecimal depth) {
        final Iterable<OrderSummary> b = summary(this.bids, OrderType.bid, precision, depth);
        final Iterable<OrderSummary> a = summary(this.asks, OrderType.ask, precision, depth);

        return new OrderBookSummary() {

            @Override
            public Iterable<OrderSummary> getAsks() {
                return a;
            }

            @Override
            public Iterable<OrderSummary> getBids() {
                return b;
            }
        };
    }

    public synchronized void initialize(Long seq, List<Order> asksList, List<Order> bidsList) {
        logger.info("initializing order book at seq {}", seq);
        for (final Order order : asksList) {
            OrderBook.put(this.asks, order.getPrice(), order.getAmount(), seq);
        }
        for (final Order order : bidsList) {
            OrderBook.put(this.bids, order.getPrice(), order.getAmount(), seq);
        }
    }

    public synchronized void processItems(Long seq, List<AmountProvider> items) {
        logger.info("queuing seq {}", seq);
        for (final AmountProvider ap : items) {
            final BigDecimal price = ap.getRate();
            final BigDecimal amount = ap.getAmount();
            final OrderType orderType = ap.getType();
            final Map<BigDecimal, BigDecimalWithSeq> map = orderType.equals(OrderType.ask) ? this.asks : this.bids;
            OrderBook.put(map, price, amount, seq);
        }
    }

    private Iterable<OrderSummary> summary(ConcurrentSkipListMap<BigDecimal, BigDecimalWithSeq> bids2, OrderType orderType, BigDecimal precision, BigDecimal depth) {
        return new Iterable<OrderSummary>() {

            private Map.Entry<BigDecimal, BigDecimalWithSeq> carryOver;

            @Override
            public Iterator<OrderSummary> iterator() {
                final Iterator<Map.Entry<BigDecimal, BigDecimalWithSeq>> it = bids2.entrySet().iterator();

                while (it.hasNext()) {
                    final Map.Entry<BigDecimal, BigDecimalWithSeq> e = it.next();
                    if (e.getValue().amount.compareTo(BigDecimal.ZERO) > 0) {
                        this.carryOver = e;
                        break;
                    }
                }
                return new Iterator<OrderSummary>() {

                    BigDecimal accumulatedAmount = BigDecimal.ZERO;

                    @Override
                    public boolean hasNext() {
                        return carryOver != null && this.accumulatedAmount.compareTo(depth) < 0;
                    }

                    @Override
                    public OrderSummary next() {
                        final OrderSummary order = new OrderSummary();
                        order.setPrice(p(carryOver));
                        order.setAssetAmount(carryOver.getValue().amount);
                        order.setMarketAmount(carryOver.getKey().multiply(carryOver.getValue().amount));
                        while (true) {
                            if (it.hasNext()) {
                                final Map.Entry<BigDecimal, BigDecimalWithSeq> next = it.next();
                                if (next.getValue().amount.compareTo(BigDecimal.ZERO) <= 0) {
                                    continue;
                                }
                                final BigDecimal rawPrice = next.getKey();
                                final BigDecimal rawAmount = next.getValue().amount;
                                final BigDecimal price = p(next);
                                if (price.compareTo(order.getPrice()) == 0) {
                                    order.setAssetAmount(order.getAssetAmount().add(rawAmount));
                                    order.setMarketAmount(order.getMarketAmount().add((rawPrice.multiply(rawAmount))));
                                    this.accumulatedAmount = next.getKey().multiply(next.getValue().amount);
                                } else {
                                    carryOver = next;
                                    break;
                                }
                            } else {
                                carryOver = null;
                                break;
                            }
                        }
                        this.accumulatedAmount = this.accumulatedAmount.add(order.getMarketAmount());
                        return order;
                    }
                };
            }

            private BigDecimal p(Map.Entry<BigDecimal, BigDecimalWithSeq> e) {
                BigDecimal price = e.getKey().divideToIntegralValue(precision);
                price = orderType.equals(OrderType.ask) ? (price.setScale(0, RoundingMode.CEILING)) : (price.setScale(0, RoundingMode.FLOOR));
                price = price.multiply(precision);
                return price;
            }
        };
    }

}
