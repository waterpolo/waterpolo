package com.strongfellow.waterpolo.push;

import java.math.BigDecimal;

import com.strongfellow.waterpolo.api.data.OrderType;

public class OrderBookRemoveItem implements AmountProvider {

    private BigDecimal rate;
    private OrderType type;

    @Override
    public BigDecimal getAmount() {
        return BigDecimal.ZERO;
    }
    @Override
    public BigDecimal getRate() {
        return this.rate;
    }

    @Override
    public OrderType getType() {
        return this.type;
    }
    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public void setType(OrderType type) {
        this.type = type;
    }

}
