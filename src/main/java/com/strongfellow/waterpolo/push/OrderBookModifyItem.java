package com.strongfellow.waterpolo.push;

import java.math.BigDecimal;

import com.strongfellow.waterpolo.api.data.OrderType;

public class OrderBookModifyItem implements AmountProvider {
    private BigDecimal rate;
    private OrderType type;
    private BigDecimal amount;

    @Override
    public BigDecimal getAmount() {
        return this.amount;
    }
    @Override
    public BigDecimal getRate() {
        return this.rate;
    }

    @Override
    public OrderType getType() {
        return this.type;
    }


    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }
    public void setType(OrderType type) {
        this.type = type;
    }
}
