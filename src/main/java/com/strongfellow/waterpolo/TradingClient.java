package com.strongfellow.waterpolo;

import com.strongfellow.waterpolo.api.ActiveLoansRequest;
import com.strongfellow.waterpolo.api.ActiveLoansResponse;
import com.strongfellow.waterpolo.api.AvailableAccountBalancesRequest;
import com.strongfellow.waterpolo.api.AvailableAccountBalancesResponse;
import com.strongfellow.waterpolo.api.BuyRequest;
import com.strongfellow.waterpolo.api.BuyResponse;
import com.strongfellow.waterpolo.api.CancelLoanOfferRequest;
import com.strongfellow.waterpolo.api.CancelLoanOfferResponse;
import com.strongfellow.waterpolo.api.CancelOrderRequest;
import com.strongfellow.waterpolo.api.CancelOrderResponse;
import com.strongfellow.waterpolo.api.CompleteBalancesRequest;
import com.strongfellow.waterpolo.api.CompleteBalancesResponse;
import com.strongfellow.waterpolo.api.CreateLoanOfferRequest;
import com.strongfellow.waterpolo.api.CreateLoanOfferResponse;
import com.strongfellow.waterpolo.api.DepositAddressesRequest;
import com.strongfellow.waterpolo.api.DepositAddressesResponse;
import com.strongfellow.waterpolo.api.DepositsWithdrawalsRequest;
import com.strongfellow.waterpolo.api.DepositsWithdrawalsResponse;
import com.strongfellow.waterpolo.api.FeeInfoRequest;
import com.strongfellow.waterpolo.api.FeeInfoResponse;
import com.strongfellow.waterpolo.api.GenerateNewAddressRequest;
import com.strongfellow.waterpolo.api.GenerateNewAddressResponse;
import com.strongfellow.waterpolo.api.LendingHistoryRequest;
import com.strongfellow.waterpolo.api.LendingHistoryResponse;
import com.strongfellow.waterpolo.api.MoveOrderRequest;
import com.strongfellow.waterpolo.api.MoveOrderResponse;
import com.strongfellow.waterpolo.api.OpenLoanOffersRequest;
import com.strongfellow.waterpolo.api.OpenLoanOffersResponse;
import com.strongfellow.waterpolo.api.OpenOrdersRequest;
import com.strongfellow.waterpolo.api.OpenOrdersResponse;
import com.strongfellow.waterpolo.api.OrderTradeRequest;
import com.strongfellow.waterpolo.api.OrderTradesResponse;
import com.strongfellow.waterpolo.api.SellRequest;
import com.strongfellow.waterpolo.api.SellResponse;
import com.strongfellow.waterpolo.api.ToggleAutoRenewRequest;
import com.strongfellow.waterpolo.api.ToggleAutoRenewResponse;
import com.strongfellow.waterpolo.api.TradeHistoryRequest;
import com.strongfellow.waterpolo.api.TradeHistoryResponse;
import com.strongfellow.waterpolo.api.WithdrawRequest;
import com.strongfellow.waterpolo.api.WithdrawResponse;
import com.strongfellow.waterpolo.exception.WaterPoloException;

public interface TradingClient {
    BuyResponse buy(BuyRequest request, AuthCredentials credentials)
            throws WaterPoloException;
    CancelLoanOfferResponse cancelLoanOffer(CancelLoanOfferRequest request, AuthCredentials credentials)
        throws WaterPoloException;
    CancelOrderResponse cancelOrder(CancelOrderRequest request, AuthCredentials credentials)
        throws WaterPoloException;
    CreateLoanOfferResponse createLoanOffer(CreateLoanOfferRequest request, AuthCredentials credentials)
        throws WaterPoloException;
    GenerateNewAddressResponse generateNewAddress(GenerateNewAddressRequest request, AuthCredentials credentials)
        throws WaterPoloException;
    MoveOrderResponse moveOrder(MoveOrderRequest request, AuthCredentials credentials)
        throws WaterPoloException;
    ActiveLoansResponse returnActiveLoans(ActiveLoansRequest request, AuthCredentials credentials)
        throws WaterPoloException;
    AvailableAccountBalancesResponse returnAvailableAccountBalances(AvailableAccountBalancesRequest request, AuthCredentials credentials)
        throws WaterPoloException;
    CompleteBalancesResponse returnCompleteBalances(CompleteBalancesRequest request, AuthCredentials credentials)
        throws WaterPoloException;
    DepositAddressesResponse returnDepositAddresses(DepositAddressesRequest request, AuthCredentials credentials)
        throws WaterPoloException;
    DepositsWithdrawalsResponse returnDepositsWithdrawals(DepositsWithdrawalsRequest request, AuthCredentials credentials)
        throws WaterPoloException;
    FeeInfoResponse returnFeeInfo(FeeInfoRequest request, AuthCredentials credentials)
        throws WaterPoloException;
    LendingHistoryResponse returnLendingHistory(LendingHistoryRequest request, AuthCredentials credentials)
        throws WaterPoloException;
    OpenLoanOffersResponse returnOpenLoanOffers(OpenLoanOffersRequest request, AuthCredentials credentials)
        throws WaterPoloException;
    OpenOrdersResponse returnOpenOrders(OpenOrdersRequest request, AuthCredentials credentials)
        throws WaterPoloException;
    OrderTradesResponse returnOrderTrades(OrderTradeRequest request, AuthCredentials credentials)
        throws WaterPoloException;
    TradeHistoryResponse returnTradeHistory(TradeHistoryRequest request, AuthCredentials credentials)
        throws WaterPoloException;
    SellResponse sell(SellRequest request, AuthCredentials credentials)
        throws WaterPoloException;
    ToggleAutoRenewResponse toggleAutoRenew(ToggleAutoRenewRequest request, AuthCredentials credentials)
        throws WaterPoloException;
    WithdrawResponse withdraw(WithdrawRequest request, AuthCredentials credentials)
        throws WaterPoloException;
}
