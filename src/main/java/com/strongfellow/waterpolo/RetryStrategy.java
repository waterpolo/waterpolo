package com.strongfellow.waterpolo;

import com.strongfellow.waterpolo.exception.WaterPoloException;

public interface RetryStrategy {

    public static RetryStrategy exponentialBackoff(int maxAttempts, long backoffInterval) {
        return (e, attempts) -> new RetryDecision(
                e.isRetryable() && (attempts < maxAttempts),
                (long) ((0.9 + 0.1 * Math.random()) * (backoffInterval * Math.pow(2, attempts))));
    }

    RetryDecision test(WaterPoloException e, long duration);
}
