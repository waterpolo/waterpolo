package com.strongfellow.waterpolo.api.data;

import java.math.BigDecimal;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.strongfellow.waterpolo.push.TradeType;

public class Trade {

    private Long tradeID;
    private Long globalTradeID;
    private BigDecimal rate;
    private BigDecimal amount;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private DateTime date;
    private BigDecimal total;
    private TradeType type;
    public BigDecimal getAmount() {
        return this.amount;
    }
    public DateTime getDate() {
        return this.date;
    }

    public Long getGlobalTradeID() {
        return this.globalTradeID;
    }
    public BigDecimal getRate() {
        return this.rate;
    }
    public BigDecimal getTotal() {
        return this.total;
    }
    public Long getTradeID() {
        return this.tradeID;
    }
    public TradeType getType() {
        return this.type;
    }
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    public void setDate(DateTime date) {
        this.date = date;
    }
    public void setGlobalTradeID(Long globalTradeID) {
        this.globalTradeID = globalTradeID;
    }
    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }
    public void setTotal(BigDecimal total) {
        this.total = total;
    }
    public void setTradeID(Long tradeID) {
        this.tradeID = tradeID;
    }
    public void setType(TradeType type) {
        this.type = type;
    }

}
