package com.strongfellow.waterpolo.api.data;

import java.math.BigDecimal;

public class OrderSummary {

    private BigDecimal price;
    private BigDecimal assetAmount;
    private BigDecimal marketAmount;
    public BigDecimal getAssetAmount() {
        return this.assetAmount;
    }
    public BigDecimal getMarketAmount() {
        return this.marketAmount;
    }
    public BigDecimal getPrice() {
        return this.price;
    }
    public void setAssetAmount(BigDecimal assetAmount) {
        this.assetAmount = assetAmount;
    }
    public void setMarketAmount(BigDecimal marketAmount) {
        this.marketAmount = marketAmount;
    }
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

}
