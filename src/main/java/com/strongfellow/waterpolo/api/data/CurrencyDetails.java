package com.strongfellow.waterpolo.api.data;

import java.math.BigDecimal;

public class CurrencyDetails {

    Long id;
    String name;
    BigDecimal txFee;
    Integer minConf;
    String depositAddress;
    Boolean disabled;
    Boolean delisted;
    Boolean frozen;
    public Boolean getDelisted() {
        return this.delisted;
    }
    public String getDepositAddress() {
        return this.depositAddress;
    }
    public Boolean getDisabled() {
        return this.disabled;
    }
    public Boolean getFrozen() {
        return this.frozen;
    }
    public Long getId() {
        return this.id;
    }
    public Integer getMinConf() {
        return this.minConf;
    }
    public String getName() {
        return this.name;
    }
    public BigDecimal getTxFee() {
        return this.txFee;
    }
    public void setDelisted(Integer delisted) {
        this.delisted = delisted == 1;
    }
    public void setDepositAddress(String depositAddress) {
        this.depositAddress = depositAddress;
    }
    public void setDisabled(Integer disabled) {
        this.disabled = disabled == 1;
    }
    public void setFrozen(Integer frozen) {
        this.frozen = frozen == 1;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public void setMinConf(Integer minConf) {
        this.minConf = minConf;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setTxFee(BigDecimal txFee) {
        this.txFee = txFee;
    }
}
