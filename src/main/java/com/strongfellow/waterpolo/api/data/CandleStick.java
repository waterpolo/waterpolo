package com.strongfellow.waterpolo.api.data;

import java.math.BigDecimal;

import org.joda.time.DateTime;

public class CandleStick {

    private DateTime date;
    private BigDecimal high;
    private BigDecimal low;
    private BigDecimal open;
    private BigDecimal close;
    private BigDecimal volume;
    private BigDecimal quoteVolume;
    private BigDecimal weightedAverage;
    public BigDecimal getClose() {
        return this.close;
    }
    public DateTime getDate() {
        return this.date;
    }
    public BigDecimal getHigh() {
        return this.high;
    }
    public BigDecimal getLow() {
        return this.low;
    }
    public BigDecimal getOpen() {
        return this.open;
    }
    public BigDecimal getQuoteVolume() {
        return this.quoteVolume;
    }
    public BigDecimal getVolume() {
        return this.volume;
    }
    public BigDecimal getWeightedAverage() {
        return this.weightedAverage;
    }
    public void setClose(BigDecimal close) {
        this.close = close;
    }
    public void setDate(DateTime date) {
        this.date = date;
    }
    public void setHigh(BigDecimal high) {
        this.high = high;
    }
    public void setLow(BigDecimal low) {
        this.low = low;
    }
    public void setOpen(BigDecimal open) {
        this.open = open;
    }
    public void setQuoteVolume(BigDecimal quoteVolume) {
        this.quoteVolume = quoteVolume;
    }
    public void setVolume(BigDecimal volume) {
        this.volume = volume;
    }
    public void setWeightedAverage(BigDecimal weightedAverage) {
        this.weightedAverage = weightedAverage;
    }

}
