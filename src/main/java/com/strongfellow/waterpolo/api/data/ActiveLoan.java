package com.strongfellow.waterpolo.api.data;

import java.math.BigDecimal;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ActiveLoan {
    private long id;
    String currency;
    BigDecimal rate;
    BigDecimal amount;
    int duration;
    int autoRenew;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    DateTime date;
    BigDecimal fees;
    public BigDecimal getAmount() {
        return this.amount;
    }
    public int getAutoRenew() {
        return this.autoRenew;
    }
    public String getCurrency() {
        return this.currency;
    }
    public DateTime getDate() {
        return this.date;
    }
    public int getDuration() {
        return this.duration;
    }
    public BigDecimal getFees() {
        return this.fees;
    }
    public long getId() {
        return this.id;
    }
    public BigDecimal getRate() {
        return this.rate;
    }
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    public void setAutoRenew(int autoRenew) {
        this.autoRenew = autoRenew;
    }
    public void setCurrency(String c) {
        this.currency = c;
    }
    public void setDate(DateTime date) {
        this.date = date;
    }
    public void setDuration(int duration) {
        this.duration = duration;
    }
    public void setFees(BigDecimal fees) {
        this.fees = fees;
    }

    public void setId(long id) {
        this.id = id;
    }
    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }
    }
