package com.strongfellow.waterpolo.api.data;

import java.math.BigDecimal;

public class TickerItem {

    private long id;
    private CurrencyPair currencyPair;
    private BigDecimal last;
    private BigDecimal lowestAsk;
    private BigDecimal highestBid;
    private BigDecimal percentChange;
    private BigDecimal baseVolume;
    private BigDecimal quoteVolume;
    private boolean isFrozen;
    private BigDecimal high24hr;
    private BigDecimal low24hr;

    public BigDecimal getBaseVolume() {
        return this.baseVolume;
    }

    public CurrencyPair getCurrencyPair() {
        return this.currencyPair;
    }

    public BigDecimal getHigh24hr() {
        return this.high24hr;
    }

    public BigDecimal getHighestBid() {
        return this.highestBid;
    }
    public long getId() {
        return this.id;
    }

    public boolean getIsFrozen() {
        return this.isFrozen;
    }
    public BigDecimal getLast() {
        return this.last;
    }
    public BigDecimal getLow24hr() {
        return this.low24hr;
    }
    public BigDecimal getLowestAsk() {
        return this.lowestAsk;
    }

    public BigDecimal getPercentChange() {
        return this.percentChange;
    }

    public BigDecimal getQuoteVolume() {
        return this.quoteVolume;
    }

    public void setBaseVolume(BigDecimal baseVolume) {
        this.baseVolume = baseVolume;
    }

    public void setCurrencyPair(String currencyPair) {
        this.currencyPair = CurrencyPair.fromString(currencyPair);
    }
    public void setHigh24hr(BigDecimal high24hr) {
        this.high24hr = high24hr;
    }
    public void setHighestBid(BigDecimal highestBid) {
        this.highestBid = highestBid;
    }
    public void setId(long id) {
        this.id = id;
    }
    public void setIsFrozen(int isFrozen) {
        this.isFrozen = isFrozen != 0;
    }
    public void setLast(BigDecimal last) {
        this.last = last;
    }
    public void setLow24hr(BigDecimal low24hr) {
        this.low24hr = low24hr;
    }
    public void setLowestAsk(BigDecimal lowestAsk) {
        this.lowestAsk = lowestAsk;
    }
    public void setPercentChange(BigDecimal percentChange) {
        this.percentChange = percentChange;
    }
    public void setQuoteVolume(BigDecimal quoteVolume) {
        this.quoteVolume = quoteVolume;
    }

}
