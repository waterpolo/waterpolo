package com.strongfellow.waterpolo.api.data;

import java.math.BigDecimal;

public class Loan {

    public Loan() {
    }
    
    private BigDecimal rate;
    private BigDecimal amount;
    private int rangeMin;
    private int rangeMax;
    public BigDecimal getRate() {
        return rate;
    }
    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }
    public BigDecimal getAmount() {
        return amount;
    }
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    public int getRangeMin() {
        return rangeMin;
    }
    public void setRangeMin(int RangeMin) {
        this.rangeMin = RangeMin;
    }
    public int getRangeMax() {
        return rangeMax;
    }
    public void setRangeMax(int RangeMax) {
        this.rangeMax = RangeMax;
    }
}
