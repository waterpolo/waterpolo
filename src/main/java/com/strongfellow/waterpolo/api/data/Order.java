package com.strongfellow.waterpolo.api.data;

import java.math.BigDecimal;

public class Order {

    private OrderType type = null;
    private BigDecimal price = null;
    private BigDecimal amount = null;

    public BigDecimal getAmount() {
        return this.amount;
    }
    public BigDecimal getPrice() {
        return this.price;
    }
    public OrderType getType() {
        return this.type;
    }
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    public void setType(OrderType type) {
        this.type = type;
    }
}
