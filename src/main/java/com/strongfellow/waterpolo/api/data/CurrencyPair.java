package com.strongfellow.waterpolo.api.data;

public class CurrencyPair {

    public static CurrencyPair fromString(String currencyPair) {
        final CurrencyPair c = new CurrencyPair();
        c.currencyPair = currencyPair;
        final int sep = currencyPair.indexOf('_');
        c.setAssetCurrency(currencyPair.substring(sep + 1));
        c.setMarketCurrency(currencyPair.substring(0, sep));
        return c;
    }

    private String currencyPair;
    private String marketCurrency;
    private String assetCurrency;

    private CurrencyPair() {}
    public String getAssetCurrency() {
        return this.assetCurrency;
    }
    public String getMarketCurrency() {
        return this.marketCurrency;
    }
    public void setAssetCurrency(String assetCurrency) {
        this.assetCurrency = assetCurrency;
    }

    public void setMarketCurrency(String marketCurrency) {
        this.marketCurrency = marketCurrency;
    }

    @Override
    public String toString() {
        return this.currencyPair;
    }

}
