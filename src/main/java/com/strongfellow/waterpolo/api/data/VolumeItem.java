package com.strongfellow.waterpolo.api.data;

import java.math.BigDecimal;

public class VolumeItem {

    private CurrencyPair currencyPair;
    private BigDecimal marketVolume;
    private BigDecimal assetVolume;

    public BigDecimal getAssetVolume() {
        return this.assetVolume;
    }
    public CurrencyPair getCurrencyPair() {
        return this.currencyPair;
    }
    public BigDecimal getMarketVolume() {
        return this.marketVolume;
    }
    public void setAssetVolume(BigDecimal assetVolume) {
        this.assetVolume = assetVolume;
    }
    public void setCurrencyPair(CurrencyPair currencyPair) {
        this.currencyPair = currencyPair;
    }
    public void setMarketVolume(BigDecimal marketVolume) {
        this.marketVolume = marketVolume;
    }
}
