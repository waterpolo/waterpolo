package com.strongfellow.waterpolo.api.data;

import java.math.BigDecimal;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

public class LoanHistory {

    Long id;
    String currency;
    BigDecimal rate;
    BigDecimal amount;
    BigDecimal duration;
    BigDecimal interest;
    BigDecimal fee;
    BigDecimal earned;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    DateTime open;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    DateTime close;
    public BigDecimal getAmount() {
        return this.amount;
    }
    public DateTime getClose() {
        return this.close;
    }
    public String getCurrency() {
        return this.currency;
    }
    public BigDecimal getDuration() {
        return this.duration;
    }
    public BigDecimal getEarned() {
        return this.earned;
    }
    public BigDecimal getFee() {
        return this.fee;
    }
    public Long getId() {
        return this.id;
    }
    public BigDecimal getInterest() {
        return this.interest;
    }
    public DateTime getOpen() {
        return this.open;
    }
    public BigDecimal getRate() {
        return this.rate;
    }
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    public void setClose(DateTime close) {
        this.close = close;
    }
    public void setCurrency(String currency) {
        this.currency = currency;
    }
    public void setDuration(BigDecimal duration) {
        this.duration = duration;
    }
    public void setEarned(BigDecimal earned) {
        this.earned = earned;
    }
    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public void setInterest(BigDecimal interest) {
        this.interest = interest;
    }
    public void setOpen(DateTime open) {
        this.open = open;
    }
    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

}
