package com.strongfellow.waterpolo.api.data;

import java.math.BigDecimal;

public class CompleteBalanceSummary {
    private BigDecimal available;
    private BigDecimal onOrders;
    private BigDecimal btcValue;
    public BigDecimal getAvailable() {
        return this.available;
    }
    public BigDecimal getBtcValue() {
        return this.btcValue;
    }
    public BigDecimal getOnOrders() {
        return this.onOrders;
    }
    public void setAvailable(BigDecimal available) {
        this.available = available;
    }
    public void setBtcValue(BigDecimal btcValue) {
        this.btcValue = btcValue;
    }
    public void setOnOrders(BigDecimal onOrders) {
        this.onOrders = onOrders;
    }
}
