package com.strongfellow.waterpolo.api.data;

public enum OrderType {
    ask,
    bid
}
