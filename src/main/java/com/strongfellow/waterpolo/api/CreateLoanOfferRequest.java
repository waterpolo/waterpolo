package com.strongfellow.waterpolo.api;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class CreateLoanOfferRequest extends WaterPoloRequest {

    private String currency;

    private BigDecimal amount;
    private int duration;

    private int autoRenew;

    private BigDecimal lendingRate;

    public CreateLoanOfferRequest() {
        super("createLoanOffer");
    }

    public BigDecimal getAmount() {
        return this.amount;
    }

    public int getAutoRenew() {
        return this.autoRenew;
    }

    public String getCurrency() {
        return this.currency;
    }

    public int getDuration() {
        return this.duration;
    }

    public BigDecimal getLendingRate() {
        return this.lendingRate;
    }

    @Override
    public List<NameValuePair> params() {
        return Arrays.asList(
                new BasicNameValuePair("currency", this.currency),
                new BasicNameValuePair("amount", String.format("%.9f", this.amount)),
                new BasicNameValuePair("duration", String.valueOf(this.duration)),
                new BasicNameValuePair("autoRenew", String.valueOf(getAutoRenew())),
                new BasicNameValuePair("lendingRate", String.format("%.9f", this.lendingRate))
                );
    }
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    public void setAutoRenew(int autoRenew) {
        this.autoRenew = autoRenew;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void setLendingRate(BigDecimal lendingRate) {
        this.lendingRate = lendingRate;
    }

}
