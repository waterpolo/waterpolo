package com.strongfellow.waterpolo.api;

import java.util.List;

import com.strongfellow.waterpolo.api.data.LoanHistory;

public class LendingHistoryResponse extends WaterPoloResponse {

    private List<LoanHistory> history;

    public List<LoanHistory> getHistory() {
        return history;
    }

    public void setHistory(List<LoanHistory> history) {
        this.history = history;
    }
}
