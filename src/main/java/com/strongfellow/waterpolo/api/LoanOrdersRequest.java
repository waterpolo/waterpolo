package com.strongfellow.waterpolo.api;

import java.util.Arrays;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class LoanOrdersRequest extends WaterPoloRequest {
    private String currency;

    public LoanOrdersRequest() {
        super("returnLoanOrders");
    }


    public String getCurrency() {
        return this.currency;
    }

    @Override
    public final List<NameValuePair> params() {
        return Arrays.asList(
                new BasicNameValuePair("currency", this.currency));
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

}
