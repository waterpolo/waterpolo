package com.strongfellow.waterpolo.api;

import java.util.Collections;
import java.util.List;

import org.apache.http.NameValuePair;

public class VolumeRequest extends WaterPoloRequest {

    public VolumeRequest() {
        super("return24hVolume");
    }

    @Override
    public List<NameValuePair> params() {
        return Collections.EMPTY_LIST;
    }

}
