package com.strongfellow.waterpolo.api;

public class GenerateNewAddressResponse extends WaterPoloResponse {

    private Integer success;
    private String response;
    private String address;

    public String getAddress() {
        return this.address;
    }
    public String getResponse() {
        return this.response;
    }
    public Integer getSuccess() {
        return this.success;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public void setResponse(String response) {
        this.response = response;
    }
    public void setSuccess(Integer success) {
        this.success = success;
    }
}
