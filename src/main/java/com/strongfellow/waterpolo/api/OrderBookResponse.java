package com.strongfellow.waterpolo.api;

import java.util.List;

import com.strongfellow.waterpolo.api.data.Order;

public class OrderBookResponse extends WaterPoloResponse {

    public List<Order> getAsks() {
        return asks;
    }
    public void setAsks(List<Order> asks) {
        this.asks = asks;
    }
    public List<Order> getBids() {
        return bids;
    }
    public void setBids(List<Order> bids) {
        this.bids = bids;
    }
    public Long getSeq() {
        return seq;
    }
    public void setSeq(Long seq) {
        this.seq = seq;
    }
    public boolean isFrozen() {
        return isFrozen;
    }
    public void setFrozen(boolean isFrozen) {
        this.isFrozen = isFrozen;
    }
    private List<Order> asks;
    private List<Order> bids;
    private Long seq;
    private boolean isFrozen;
}
