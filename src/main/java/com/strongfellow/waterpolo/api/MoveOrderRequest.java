package com.strongfellow.waterpolo.api;

import java.util.Collections;
import java.util.List;

import org.apache.http.NameValuePair;

public class MoveOrderRequest extends WaterPoloRequest {

    public MoveOrderRequest() {
        super("moveOrder");
    }

    @Override
    public List<NameValuePair> params() {
        return Collections.EMPTY_LIST;
    }
}
