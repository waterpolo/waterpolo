package com.strongfellow.waterpolo.api;

import java.util.Arrays;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.joda.time.DateTime;

import com.strongfellow.waterpolo.api.data.CurrencyPair;

public class TradeHistoryRequest extends WaterPoloRequest {

    private DateTime start;

    private DateTime end;
    private CurrencyPair currencyPair;
    public TradeHistoryRequest() {
        super("returnTradeHistory");
    }

    @Override
    public List<NameValuePair> params() {
        return Arrays.asList(
                new BasicNameValuePair("currencyPair", this.currencyPair.toString()),
                new BasicNameValuePair("start", String.valueOf(this.start.getMillis() / 1000)),
                new BasicNameValuePair("end", String.valueOf(this.end.getMillis() / 1000))
                );
    }


    public void setCurrencyPair(String currencyPair) {
        this.currencyPair = CurrencyPair.fromString(currencyPair);
    }

    public void setEnd(DateTime e) {
        this.end = e;
    }

    public void setStart(DateTime s) {
        this.start = s;
    }

}
