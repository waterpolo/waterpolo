package com.strongfellow.waterpolo.api;

import java.util.Arrays;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class ToggleAutoRenewRequest extends WaterPoloRequest {

    private Long orderNumber;

    public ToggleAutoRenewRequest() {
        super("toggleAutoRenew");
    }

    @Override
    public final List<NameValuePair> params() {
        return Arrays.asList(
                new BasicNameValuePair("orderNumber", String.valueOf(this.orderNumber)));
    }

    public void setOrderNumber(Long n) {
        this.orderNumber = n;
    }
}
