package com.strongfellow.waterpolo.api;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class AvailableAccountBalancesRequest extends WaterPoloRequest {

    private String account;

    public AvailableAccountBalancesRequest() {
        super("returnAvailableAccountBalances");
    }

    public String getAccount() {
        return this.account;
    }

    @Override
    public List<NameValuePair> params() {
        if (this.account == null) {
            return Collections.EMPTY_LIST;
        } else {
            return Arrays.asList(new BasicNameValuePair("account", this.account));
        }
    }

    public void setAccount(String account) {
        if (account == null || account.equals("lending") || account.equals("exchange")|| account == "lending") {
            this.account = account;
        } else {
            throw new IllegalArgumentException("invalid account " + account);
        }
    }
}
