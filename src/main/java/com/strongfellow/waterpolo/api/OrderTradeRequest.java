package com.strongfellow.waterpolo.api;

import java.util.Collections;
import java.util.List;

import org.apache.http.NameValuePair;

public class OrderTradeRequest extends WaterPoloRequest {

    public OrderTradeRequest() {
        super("orderTrade");
    }

    @Override
    public List<NameValuePair> params() {
        return Collections.EMPTY_LIST;
    }
}
