package com.strongfellow.waterpolo.api;

import java.util.Collections;
import java.util.List;

import org.apache.http.NameValuePair;

public class WithdrawRequest extends WaterPoloRequest {

    public WithdrawRequest() {
        super("withdraw");
    }

    @Override
    public final List<NameValuePair> params() {
        return Collections.EMPTY_LIST;
    }

}
