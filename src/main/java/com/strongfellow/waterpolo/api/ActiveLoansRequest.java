package com.strongfellow.waterpolo.api;

import java.util.Collections;
import java.util.List;

import org.apache.http.NameValuePair;

public class ActiveLoansRequest extends WaterPoloRequest {

    public ActiveLoansRequest() {
        super("returnActiveLoans");
    }

    @Override
    public List<NameValuePair> params() {
        return Collections.EMPTY_LIST;
    }

}
