package com.strongfellow.waterpolo.api;

import java.util.Collections;
import java.util.List;

import org.apache.http.NameValuePair;

public class OpenLoanOffersRequest extends WaterPoloRequest {

    public OpenLoanOffersRequest() {
        super("returnOpenLoanOffers");
    }

    @Override
    public List<NameValuePair> params() {
        return Collections.EMPTY_LIST;
    }

}
