package com.strongfellow.waterpolo.api;

import java.util.List;

import com.strongfellow.waterpolo.api.data.ActiveLoan;

public class ActiveLoansResponse extends WaterPoloResponse {
    private List<ActiveLoan> used;
    private List<ActiveLoan> provided;

    public List<ActiveLoan> getProvided() {
        return this.provided;
    }
    public List<ActiveLoan> getUsed() {
        return this.used;
    }
    public void setProvided(List<ActiveLoan> provided) {
        this.provided = provided;
    }
    public void setUsed(List<ActiveLoan> used) {
        this.used = used;
    }
}
