package com.strongfellow.waterpolo.api;

import java.util.List;

import org.apache.http.NameValuePair;

public abstract class WaterPoloRequest {

    private String command;

    public WaterPoloRequest(String command) {
        this.command = command;
    }

    public final String getCommand() {
        return this.command;
    }

    public abstract List<NameValuePair> params();

    public void setCommand(String c) {
        this.command = c;
    }

}
