package com.strongfellow.waterpolo.api;

import java.util.HashMap;
import java.util.Map;

public class DepositAddressesResponse extends WaterPoloResponse {

    private final Map<String, String> addresses = new HashMap<>();

    public Map<String, String> getAddresses() {
        return this.addresses;
    }
}
