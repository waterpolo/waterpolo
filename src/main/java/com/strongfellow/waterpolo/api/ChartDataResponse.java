package com.strongfellow.waterpolo.api;

import java.util.List;

import com.strongfellow.waterpolo.api.data.CandleStick;

public class ChartDataResponse extends WaterPoloResponse {
    private List<CandleStick>  candleSticks;

    public List<CandleStick> getCandleSticks() {
        return this.candleSticks;
    }

    public void setCandleSticks(List<CandleStick> candleSticks) {
        this.candleSticks = candleSticks;
    }
}
