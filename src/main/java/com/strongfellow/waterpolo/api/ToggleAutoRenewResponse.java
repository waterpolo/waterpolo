package com.strongfellow.waterpolo.api;

public class ToggleAutoRenewResponse extends WaterPoloResponse {

    private Integer success;
    private Integer message;
    public Integer getMessage() {
        return this.message;
    }
    public Integer getSuccess() {
        return this.success;
    }
    public void setMessage(Integer message) {
        this.message = message;
    }
    public void setSuccess(Integer success) {
        this.success = success;
    }
}
