package com.strongfellow.waterpolo.api;

import java.util.Collections;
import java.util.List;

import org.apache.http.NameValuePair;

public class FeeInfoRequest extends WaterPoloRequest {

    public FeeInfoRequest() {
        super("returnFeeInfo");
    }

    @Override
    public List<NameValuePair> params() {
        return Collections.EMPTY_LIST;
    }
}
