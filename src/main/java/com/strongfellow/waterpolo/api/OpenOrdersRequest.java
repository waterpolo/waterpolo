package com.strongfellow.waterpolo.api;

import java.util.Collections;
import java.util.List;

import org.apache.http.NameValuePair;

public class OpenOrdersRequest extends WaterPoloRequest {

    public OpenOrdersRequest() {
        super("returnOpenOrders");
    }

    @Override
    public List<NameValuePair> params() {
        return Collections.EMPTY_LIST;
    }
}
