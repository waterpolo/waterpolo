package com.strongfellow.waterpolo.api;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.joda.time.DateTime;

public class LendingHistoryRequest extends WaterPoloRequest {

    private Long limit = null;

    private DateTime start;
    private DateTime end;
    public LendingHistoryRequest() {
        super("returnLendingHistory");
    }


    public DateTime getEnd() {
        return this.end;
    }

    public Long getLimit() {
        return this.limit;
    }

    public DateTime getStart() {
        return this.start;
    }

    @Override
    public List<NameValuePair> params() {
        final List<NameValuePair> params = new ArrayList<>();
        if (this.start != null) {
            params.add(new BasicNameValuePair("start", String.valueOf(this.start.getMillis() / 1000)));
        }
        if (this.end != null) {
            params.add(new BasicNameValuePair("end", String.valueOf(this.end.getMillis() / 1000)));
        }
        if (this.limit != null) {
            params.add(new BasicNameValuePair("limit", String.valueOf(this.limit)));
        }
        return params;
    }

    public void setEnd(DateTime end) {
        this.end = end;
    }

    public void setLimit(Long limit) {
        this.limit = limit;
    }

    public void setStart(DateTime start) {
        this.start = start;
    }
}