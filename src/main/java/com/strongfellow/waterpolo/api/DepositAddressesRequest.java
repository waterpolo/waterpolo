package com.strongfellow.waterpolo.api;

import java.util.Collections;
import java.util.List;

import org.apache.http.NameValuePair;

public class DepositAddressesRequest extends WaterPoloRequest {

    public DepositAddressesRequest() {
        super("returnDepositAddresses");
    }

    @Override
    public List<NameValuePair> params() {
        return Collections.EMPTY_LIST;
    }

}
