package com.strongfellow.waterpolo.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.strongfellow.waterpolo.api.data.CurrencyPair;

public class OrderBookRequest extends WaterPoloRequest {

    private Long depth;

    private CurrencyPair currencyPair;
    public OrderBookRequest() {
        super("returnOrderBook");
    }

    @Override
    public List<NameValuePair> params() {
        final List<NameValuePair> params = new ArrayList<>(Arrays.asList(
                new BasicNameValuePair("currencyPair", this.currencyPair.toString())
                ));
        if (this.depth != null) {
            params.add(new BasicNameValuePair("depth", String.valueOf(this.depth)));
        }
        return params;
    }

    public void setCurrencyPair(CurrencyPair currencyPair) {
        this.currencyPair = currencyPair;
    }

    public void setCurrencyPair(String currencyPair) {
        setCurrencyPair(CurrencyPair.fromString(currencyPair));
    }

    public void setDepth(Long depth) {
        this.depth = depth;
    }

}
