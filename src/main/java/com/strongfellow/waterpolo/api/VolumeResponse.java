package com.strongfellow.waterpolo.api;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import com.strongfellow.waterpolo.api.data.CurrencyPair;
import com.strongfellow.waterpolo.api.data.VolumeItem;

public class VolumeResponse extends WaterPoloResponse {

    private final Map<String, BigDecimal> totals = new HashMap<>();
    private final Map<CurrencyPair, VolumeItem> volumes = new HashMap<>();

    public Map<String, BigDecimal> getTotals() {
        return this.totals;
    }

    public Map<CurrencyPair, VolumeItem> getVolumes() {
        return this.volumes;
    }

}
