package com.strongfellow.waterpolo.api;

import java.util.Collections;
import java.util.List;

import org.apache.http.NameValuePair;

public class BuyRequest extends WaterPoloRequest {

    public BuyRequest() {
        super("buy");
    }

    @Override
    public List<NameValuePair> params() {
        return Collections.EMPTY_LIST;
    }
}
