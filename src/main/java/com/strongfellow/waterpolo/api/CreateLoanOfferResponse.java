package com.strongfellow.waterpolo.api;

public class CreateLoanOfferResponse extends WaterPoloResponse {

    Integer success;
    String message;
    Integer orderID;

    public String getMessage() {
        return this.message;
    }
    public Integer getOrderID() {
        return this.orderID;
    }
    public Integer getSuccess() {
        return this.success;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public void setOrderID(Integer orderID) {
        this.orderID = orderID;
    }
    public void setSuccess(Integer success) {
        this.success = success;
    }
}
