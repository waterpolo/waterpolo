package com.strongfellow.waterpolo.api;

import java.util.List;

import com.strongfellow.waterpolo.api.data.Loan;

public class LoanOrdersResponse extends WaterPoloResponse {
    private List<Loan> offers;
    private List<Loan> demands;

    public List<Loan> getDemands() {
        return this.demands;
    }
    public List<Loan> getOffers() {
        return this.offers;
    }
    public void setDemands(List<Loan> demands) {
        this.demands = demands;
    }
    public void setOffers(List<Loan> offers) {
        this.offers = offers;
    }
}
