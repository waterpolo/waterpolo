package com.strongfellow.waterpolo.api;

import java.util.HashMap;
import java.util.Map;

import com.strongfellow.waterpolo.api.data.CompleteBalanceSummary;

public class CompleteBalancesResponse extends WaterPoloResponse {

    Map<String, CompleteBalanceSummary> balances = new HashMap<>();

    public Map<String, CompleteBalanceSummary> getBalances() {
        return this.balances;
    }

    public void setBalances(Map<String, CompleteBalanceSummary> balances) {
        this.balances = balances;
    }
}
