package com.strongfellow.waterpolo.api;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class CompleteBalancesRequest extends WaterPoloRequest {

    private String account;

    public CompleteBalancesRequest() {
        super("returnCompleteBalances");
    }

    public String getAccount() {
        return this.account;
    }

    @Override
    public List<NameValuePair> params() {
        if (this.account == null) {
            return Collections.EMPTY_LIST;
        } else {
            return Arrays.asList(
                    new BasicNameValuePair("account", this.account));
        }
    }

    public void setAccount(String account) {
        this.account = account;
    }


}
