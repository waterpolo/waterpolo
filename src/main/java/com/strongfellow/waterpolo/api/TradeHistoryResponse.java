package com.strongfellow.waterpolo.api;

import java.util.List;

import com.strongfellow.waterpolo.api.data.Trade;

public class TradeHistoryResponse extends WaterPoloResponse {

    private List<Trade> trades;

    public List<Trade> getTrades() {
        return this.trades;
    }

    public void setTrades(List<Trade> trades) {
        this.trades = trades;
    }
}
