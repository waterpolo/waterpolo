package com.strongfellow.waterpolo.api;

import java.util.Map;

import com.strongfellow.waterpolo.api.data.CurrencyDetails;

public class CurrenciesResponse extends WaterPoloResponse {
    private Map<String, CurrencyDetails> curencyDetails;

    public Map<String, CurrencyDetails> getCurencyDetails() {
        return this.curencyDetails;
    }

    public void setCurencyDetails(Map<String, CurrencyDetails> curencyDetails) {
        this.curencyDetails = curencyDetails;
    }
}
