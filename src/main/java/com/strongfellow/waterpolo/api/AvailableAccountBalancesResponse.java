package com.strongfellow.waterpolo.api;

import java.math.BigDecimal;
import java.util.Map;

public class AvailableAccountBalancesResponse extends WaterPoloResponse {

    Map<String, BigDecimal> exchange;
    Map<String, BigDecimal> margin;
    Map<String, BigDecimal> lending;
    public Map<String, BigDecimal> getExchange() {
        return this.exchange;
    }
    public Map<String, BigDecimal> getLending() {
        return this.lending;
    }
    public Map<String, BigDecimal> getMargin() {
        return this.margin;
    }
    public void setExchange(Map<String, BigDecimal> exchange) {
        this.exchange = exchange;
    }
    public void setLending(Map<String, BigDecimal> lending) {
        this.lending = lending;
    }
    public void setMargin(Map<String, BigDecimal> margin) {
        this.margin = margin;
    }
}
