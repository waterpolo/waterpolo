package com.strongfellow.waterpolo.api;

import java.math.BigDecimal;

public class CancelLoanOfferResponse extends WaterPoloResponse {

    Integer success;
    String message;
    BigDecimal amount;
    public Integer getSuccess() {
        return success;
    }
    public void setSuccess(Integer success) {
        this.success = success;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public BigDecimal getAmount() {
        return amount;
    }
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

}
