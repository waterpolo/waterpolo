package com.strongfellow.waterpolo.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.strongfellow.waterpolo.api.data.LoanOffer;

public class OpenLoanOffersResponse extends WaterPoloResponse {

    private final Map<String, List<LoanOffer>> offers = new HashMap<>();

    public Map<String, List<LoanOffer>> getOffers() {
        return this.offers;
    }
}
