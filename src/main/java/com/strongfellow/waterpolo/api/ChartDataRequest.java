package com.strongfellow.waterpolo.api;

import java.util.Arrays;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.joda.time.DateTime;

import com.strongfellow.waterpolo.api.data.CurrencyPair;

public class ChartDataRequest extends WaterPoloRequest {

    public enum CandleStickPeriod {
        FIVE_MINUTES(300),
        FIFTEEN_MINUTES(900),
        THIRTY_MINUTES(1800),
        TWO_HOURS(7200),
        FOUR_HOURS(14400),
        ONE_DAY(86400);

        private final int period;
        private CandleStickPeriod(int period) {
            this.period = period;
        }
    }


    private DateTime start;

    private DateTime end;
    private CandleStickPeriod candleStickPeriod;
    private CurrencyPair currencyPair;
    public ChartDataRequest() {
        super("returnChartData");
    }

    @Override
    public List<NameValuePair> params() {
        return Arrays.asList(
                new BasicNameValuePair("currencyPair", this.currencyPair.toString()),
                new BasicNameValuePair("period", String.valueOf(this.candleStickPeriod.period)),
                new BasicNameValuePair("start", String.valueOf(this.start.getMillis() / 1000)),
                new BasicNameValuePair("end", String.valueOf(this.end.getMillis() / 1000))
                );
    }

    public void setCandlestickPeriod(CandleStickPeriod period) {
        this.candleStickPeriod = period;
    }

    public void setCurrencyPair(String currencyPair) {
        this.currencyPair = CurrencyPair.fromString(currencyPair);
    }

    public void setEnd(DateTime e) {
        this.end = e;
    }


    public void setStart(DateTime s) {
        this.start = s;
    }

}
