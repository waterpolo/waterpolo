package com.strongfellow.waterpolo.api;

import java.util.Collections;
import java.util.List;

import org.apache.http.NameValuePair;

public class CurrenciesRequest extends WaterPoloRequest {

    public CurrenciesRequest() {
        super("returnCurrencies");
    }

    @Override
    public List<NameValuePair> params() {
        return Collections.EMPTY_LIST;
    }

}
