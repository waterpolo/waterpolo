package com.strongfellow.waterpolo.api;

import java.util.Arrays;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class CancelLoanOfferRequest extends WaterPoloRequest {

    private Long orderNumber;

    public CancelLoanOfferRequest() {
        super("cancelLoanOffer");
    }

    public Long getOrderNumber() {
        return this.orderNumber;
    }

    @Override
    public List<NameValuePair> params() {
        return Arrays.asList(
                new BasicNameValuePair("orderNumber", this.orderNumber.toString())
                );
    }

    public void setOrderNumber(Long orderNumber) {
        this.orderNumber = orderNumber;
    }
}
