package com.strongfellow.waterpolo.api;

import java.util.List;

import com.strongfellow.waterpolo.api.data.TickerItem;

public class TickerResponse extends WaterPoloResponse {

    private List<TickerItem> tickers;

    public List<TickerItem> getTickers() {
        return tickers;
    }

    public void setTickers(List<TickerItem> tickers) {
        this.tickers = tickers;
    }
}
