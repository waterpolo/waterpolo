package com.strongfellow.waterpolo;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClients;

public class ClientBuilder {

    private static final HttpClient httpClient = HttpClients.createDefault();
    private NonceProvider nonceProvider = () -> System.currentTimeMillis();
    private RetryStrategy retryStrategy = RetryStrategy.exponentialBackoff(5, 1000);

    public ClientBuilder() {
    }

    public OpenClient getOpenClient() {
        final OpenClient openClient = new DefaultOpenClient(httpClient, this.retryStrategy);
        return openClient;
    }

    public DefaultTradingClient getTradingClient() {
        final DefaultTradingClient tradingClient =
                new DefaultTradingClient(httpClient, this.nonceProvider, this.retryStrategy);
        return tradingClient;
    }

    public ClientBuilder withNonceProvider(NonceProvider nonceProvider) {
        this.nonceProvider = nonceProvider;
        return this;
    }

    public ClientBuilder withRetryStrategy(RetryStrategy r) {
        this.retryStrategy = r;
        return this;
    }

}
