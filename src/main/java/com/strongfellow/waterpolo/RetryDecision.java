package com.strongfellow.waterpolo;

public class RetryDecision {

    private final boolean isRetry;
    private final Long backoff;

    public RetryDecision(boolean retry, Long backoff) {
        this.isRetry = retry;
        this.backoff = backoff;
    }

    public Long getBackoff() {
        return this.backoff;
    }

    public boolean isRetry() {
        return this.isRetry;
    }
}
