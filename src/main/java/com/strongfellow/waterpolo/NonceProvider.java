package com.strongfellow.waterpolo;

public interface NonceProvider {
    Long get();
}
