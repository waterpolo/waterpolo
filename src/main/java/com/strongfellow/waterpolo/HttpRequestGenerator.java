package com.strongfellow.waterpolo;

import org.apache.http.client.methods.HttpUriRequest;

import com.strongfellow.waterpolo.exception.WaterPoloException;

public interface HttpRequestGenerator {

    HttpUriRequest generate() throws WaterPoloException;

}
